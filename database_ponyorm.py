from pony.orm import *

_db = Database()


class Feed(_db.Entity):
    id = PrimaryKey(int, auto=True)
    department = Required(str)
    n_feed = Required(int, unique=True)


_params = dict(
    sqlite=dict(provider='sqlite', filename='database.sqlite', create_db=True),
    mysql=dict(provider='mysql', host="localhost", user="pony", passwd="pony", db="pony"),
    postgres=dict(provider='postgres', user='pony', password='pony', host='localhost', database='pony'),
    oracle=dict(provider='oracle', user='c##pony', password='pony', dsn='localhost/orcl')
)

_db.bind(**_params['sqlite'])

_db.generate_mapping(create_tables=True)
