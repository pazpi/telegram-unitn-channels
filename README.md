# Unitn News for DISI, DII, DICAM

##how-to:
Create a file named .env and put the telegram bot token as

`TOKEN=<token>`

build docker image

`docker-compose build`

run image

`docker-compose up`

or in deamon mode

`docker-compose up -d`