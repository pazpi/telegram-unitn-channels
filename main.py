import feedparser
from os import getenv

from telegram.ext import Updater
from telegram.ext.dispatcher import run_async

from pony import orm

from time import sleep

# userspace
from database_ponyorm import Feed

import logging.handlers
# enable logging
f = logging.Formatter(fmt='%(levelname)s:%(name)s: %(message)s (%(asctime)s; %(filename)s:%(lineno)d)',
                      datefmt="%Y-%m-%d %H:%M:%S")
handlers = [
    logging.handlers.RotatingFileHandler('root.log', encoding='utf8', maxBytes=100000, backupCount=1),
    logging.StreamHandler()
]
log = logging.getLogger(__name__)
log.setLevel(logging.INFO)
for h in handlers:
    h.setFormatter(f)
    h.setLevel(logging.INFO)
    log.addHandler(h)

# Telegram TOKEN is a docker environment
TOKEN = getenv('TOKEN')
REFRESH_RSS_TIME = int(getenv('REFRESH_RSS_TIME'))

url = {'disi':  'http://www.science.unitn.it/cisca/avvisi/feedavvisi.html',
       'dii':   'http://www.science.unitn.it/avvisiesami/dii-cibio/feed/',
       'dicam': 'http://www.science.unitn.it/avvisiesami/dicam/feed/'
       }


@orm.db_session
def check_feed_new(department, n_feed):
    feed = Feed.get(department=department, n_feed=n_feed)
    return feed


@orm.db_session
def insert_new_feed(department, n_feed):
    feed = Feed(department=department, n_feed=n_feed)
    return feed


def filter_feeds(department, feeds):
    new_feed = []
    for feed in feeds.entries:
        # link format: 'http://www.science.unitn.it/cisca/avvisi/vediavviso.php?vavviso=6853'
        link_feed = feed['link']
        # get the last number in the link
        n_feed = [int(s) for s in link_feed.split(sep='=') if s.isdigit()][-1]
        log.debug("n_feed: {}".format(n_feed))
        if not check_feed_new(department, n_feed):
            new_feed.append(feed.title)
            insert_new_feed(department, n_feed)
    return new_feed


@run_async
def update_rss(bot, job):
    log.info('Update RSS')
    for key in url.keys():
        feeds = feedparser.parse(url[key])

        if feeds:
            log.info('Feed count {} {}'.format(key, len(feeds.entries)))
            new_feed = filter_feeds(key, feeds)
            log.info('New feed {}: {}'.format(key, len(new_feed)))
            for feed in new_feed:
                log.debug(feed)
                chat_id = '@Avvisi' + key.upper() + 'Unitn'
                bot.send_message(chat_id=chat_id, text=feed)
                sleep(1)


def main():
    # crea il gestore di eventi (EventHandler) e gli passa il token
    updater = Updater(token=TOKEN, workers=4)
    job = updater.job_queue
    log.info("Creato updater")

    log.debug('')
    job.run_repeating(update_rss, REFRESH_RSS_TIME, first=1)

    # lancia il bot
    updater.start_polling()
    log.info("Start polling")
    # fa funzionare il bot fino a quando non fai ctrl + C
    updater.idle()


if __name__ == '__main__':
    log.critical('Nuovo avvio del bot --------------------------------------------------------------------------------')
    log.critical("main")
    main()
